package com.kozlovnazarov.models;

import java.io.File;
import java.util.ArrayList;

public class WebSiteModel {
    ArrayList website;
    ArrayList usersOnline;

    public WebSiteModel() {
        usersOnline = new ArrayList<String>();
        website = new ArrayList<DocumentModel>();
        formWebsite();
    }

    public DocumentModel getDocument(String uri) {
        if (uri != null) {
            for (Object document : website) {
                if (((DocumentModel) document).getUri().equals(uri)) {
                    return ((DocumentModel) document);
                }
            }
        }
        return null;
    }

    public boolean addUserOnline(String nickname) {
        if (!(usersOnline.contains(nickname) || nickname.equals(""))) {
            usersOnline.add(nickname);
            return true;
        }
        return false;
    }

    public boolean removeUserOnline(String nickname) {
        boolean res = false;
        try {
            if (usersOnline.contains(nickname)) {
                for (Object document : website) {
                    res = ((DocumentModel) document).removeUser(nickname);
                    if (res) {
                        break;
                    }
                }
                usersOnline.remove(nickname);
            }
        } catch (NullPointerException ex) {
            return false;
        }
        return res;
    }

    public boolean attachUserToTheDocument(String nickname, String docUri) {
        try {
            if (usersOnline.contains(nickname)) {
                return getDocument(docUri).addUser(nickname);
            }
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public boolean detachUserFromTheDocument(String nickname, String docUri) {
        try {
            if (usersOnline.contains(nickname)) {
                return getDocument(docUri).removeUser(nickname);
            }
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public ArrayList getUsersOnline() {
        return usersOnline;
    }

    public ArrayList getWebsite() {
        return website;
    }

    public ArrayList getDocumentUris() {
        ArrayList uris = new ArrayList<String>();
        for (Object document : website) {
            uris.add(((DocumentModel) document).getUri());
        }
        return uris;
    }

    private void formWebsite() {
        File folder = new File("src/com/kozlovnazarov/web");
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                DocumentModel model = new DocumentModel(file.getPath());
                website.add(model);
            }
        }
    }
}
