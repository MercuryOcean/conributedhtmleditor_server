package com.kozlovnazarov.models;


import java.io.IOException;

public class StringLineModel {

    private String content;
    private String reserverNickName;

    StringLineModel(String content, String reserverNickName) {
        this.content = content;
        this.reserverNickName = reserverNickName;
    }

    public void setReserverNickName(String reserverNickName) {
        this.reserverNickName = reserverNickName;
    }

    public String getReserverNickName() {
        return reserverNickName;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
