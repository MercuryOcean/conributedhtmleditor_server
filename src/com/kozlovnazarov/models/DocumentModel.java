package com.kozlovnazarov.models;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DocumentModel {

    private String uri = "src/com/kozlovnazarov/web/index.html";
    private ArrayList document;
    private ArrayList usersOnTheDocument;

    public DocumentModel() {
        document = new ArrayList<StringLineModel>();
        usersOnTheDocument = new ArrayList<String>();
        formDocument(uri);
    }

    public DocumentModel(String uri) {
        document = new ArrayList<StringLineModel>();
        usersOnTheDocument = new ArrayList<String>();
        String new_uri = uri.replace("\\", "/");
        formDocument(new_uri);
    }

    public boolean addStringLine(int id, String content, String reserverNickName) {
        try {
            if (!reserverNickName.contains(reserverNickName)) {
                return false;
            }
        } catch (NullPointerException ex) {
        }
        StringLineModel stringLineModel = new StringLineModel(content, reserverNickName);
        document.add(id, stringLineModel);
        saveToFile();
        return true;
    }

    public boolean addUser(String nickName) {
        if (!(usersOnTheDocument.contains(nickName) || nickName.equals(""))) {
            usersOnTheDocument.add(nickName);
            return true;
        }
        return false;
    }

    public boolean removeUser(String userNickName) {
        boolean res = false;
        for (Object stringLineModel : document) {
            if (userNickName.equals(((StringLineModel) stringLineModel).getReserverNickName())){
                ((StringLineModel) stringLineModel).setReserverNickName(null);
                res = true;
            }
        }
        usersOnTheDocument.remove(userNickName);
        return res;
    }

    public boolean removeLine(int id) {
        document.remove(id);
        return saveToFile();
    }

    public String lineReserverNickName(int id) {
        return ((StringLineModel) document.get(id)).getReserverNickName();
    }

    public boolean setLineReserver(int id, String reserverNickName) {
        if (!usersOnTheDocument.contains(reserverNickName)) {
            return false;
        }
        ((StringLineModel) document.get(id)).setReserverNickName(reserverNickName);
        return true;
    }

    public List<Integer> getLinesIdxReservedBy(String reserverNickName) {
        List idList = new ArrayList<Integer>();
        int idx = 0;
        for (Object stringLineModel : document) {
            try {
                if (((StringLineModel) stringLineModel).getReserverNickName().equals(reserverNickName)) {
                    idList.add((Integer) idx);
                }
            } catch (NullPointerException ex) {
            }
            idx++;
        }
        return idList;
    }

    public List<String> getLinesContentReservedBy(String reserverNickName) {
        List contentList = new ArrayList<String>();
        int idx = 0;
        for (Object stringLineModel : document) {
            try {
                if (((StringLineModel) stringLineModel).getReserverNickName().equals(reserverNickName)) {
                    contentList.add(((StringLineModel) stringLineModel).getContent());
                }
            } catch (NullPointerException ex) {
            }
            idx++;
        }
        return contentList;
    }

    public String getUri() {
        return uri;
    }

    private void formDocument(String uri) {
        this.uri = uri;
        Path htmlDocPath = Paths.get(this.uri);
        Charset charset = Charset.forName("UTF-8");
        try {
            List<String> lines = Files.readAllLines(htmlDocPath, charset);
            for (String line : lines) {
                StringLineModel stringLineModel = new StringLineModel(line, null);
                document.add(stringLineModel);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<String> getUsers() {
        return usersOnTheDocument;
    }

    private boolean saveToFile() {
        String outString = "";
        for (Object stringLineModel : document) {
            outString = outString.concat(((StringLineModel) stringLineModel).getContent() + "\n");
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(uri));
            writer.write(outString);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return true;
    }

    @Override
    public String toString() {
        String outString = "";
        for (Object stringLineModel : document) {
            outString = outString.concat(((StringLineModel) stringLineModel).getContent() + "\n");
        }
        return outString;
    }
}
