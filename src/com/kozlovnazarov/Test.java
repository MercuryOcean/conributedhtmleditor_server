package com.kozlovnazarov;

import com.kozlovnazarov.models.DocumentModel;
import com.kozlovnazarov.models.WebSiteModel;

import java.util.List;

public class Test {

    public static void main(String[] args) {



        /***** DOCUMENTMODEL TESTS *****/

        // TEST CASE : CONSTRUCTOR
        DocumentModel documentModelTests_model = new DocumentModel("src/com/kozlovnazarov/web/test.html");
        String documentModelTests_str = documentModelTests_model.toString();
        System.out.println(documentModelTests_str);

        // TEST CASE : ADD LINE
        documentModelTests_model.addStringLine(1, "<!-- Write your comments here -->", null);
        documentModelTests_str = documentModelTests_model.toString();
        System.out.println(documentModelTests_str);

        // TEST CASE : REMOVE LINE
        documentModelTests_model.removeLine(1);
        documentModelTests_str = documentModelTests_model.toString();
        System.out.println(documentModelTests_str);

        // TEST CASE : GET RESERVER
        documentModelTests_str = documentModelTests_model.lineReserverNickName(0);
        System.out.println(documentModelTests_str);

        // TEST CASE : GET RESERVER
        documentModelTests_model.addUser("initUser");
        documentModelTests_model.setLineReserver(0,"initUser");
        documentModelTests_str = documentModelTests_model.lineReserverNickName(0);
        System.out.println(documentModelTests_str);

        // TEST CASE : GET RESERVED STRING
        documentModelTests_model.setLineReserver(0,"initUser");
        documentModelTests_model.setLineReserver(1,"initUser");
        documentModelTests_model.setLineReserver(2,"initUser");
        List list = documentModelTests_model.getLinesIdxReservedBy("initUser");
        System.out.println(list.toString());

        //TEST CASE : REMOVE USER
        documentModelTests_model.removeUser("initUser");
        documentModelTests_str = documentModelTests_model.lineReserverNickName(0);
        System.out.println(documentModelTests_str);

        //TEST CASE : RE-INDEX
        documentModelTests_model.addUser("initUser2");
        documentModelTests_model.setLineReserver(3, "initUser2");
        System.out.println(documentModelTests_model.getLinesContentReservedBy("initUser2"));
        documentModelTests_model.addStringLine(3, "<!-- Write your comments here -->", null);
        System.out.println(documentModelTests_model.getLinesContentReservedBy("initUser2"));
        documentModelTests_model.removeLine(3);
        System.out.println(documentModelTests_model.getLinesContentReservedBy("initUser2"));
        documentModelTests_model.removeUser("initUser2");

        //TEST CASE : GET URI
        System.out.println(documentModelTests_model.getUri());

        // TEST CASE : GET USERS ON THE DOCUMENT
        documentModelTests_model.addUser("0");
        documentModelTests_model.addUser("1");
        documentModelTests_model.addUser("2");
        documentModelTests_model.addUser("3");
        System.out.println(documentModelTests_model.getUsers());
        documentModelTests_model.removeUser("2");
        System.out.println(documentModelTests_model.getUsers());



        /***** WEBSITEMODEL TESTS *****/

        // TEST CASE : CONSTRUCTOR
        WebSiteModel webSiteModelTests_model = new WebSiteModel();

        // TEST CASE : GET URIS
        System.out.println(webSiteModelTests_model.getDocumentUris());

        // TEST CASE : ADD USER ONLINE
        webSiteModelTests_model.addUserOnline("init0");
        webSiteModelTests_model.addUserOnline("init1");
        webSiteModelTests_model.addUserOnline("init2");

        // TEST CASE : GET USERS ONLINE
        System.out.println(webSiteModelTests_model.getUsersOnline());

        // TEST CASE : REMOVE USER ONLINE
        webSiteModelTests_model.removeUserOnline("init1");
        System.out.println(webSiteModelTests_model.getUsersOnline());
        webSiteModelTests_model.removeUserOnline("init0");
        webSiteModelTests_model.removeUserOnline("init2");

        // TEST CASE : ATTACH USER TO THE DOCUMENT
        webSiteModelTests_model.addUserOnline("init");
        webSiteModelTests_model.attachUserToTheDocument("init", "src/com/kozlovnazarov/web/index.html");
        DocumentModel tmp_model = webSiteModelTests_model.getDocument("src/com/kozlovnazarov/web/index.html");
        System.out.println(tmp_model.getUsers());

        // TEST CASE : REMOVE USER
        webSiteModelTests_model.removeUserOnline("init");
        System.out.println(tmp_model.getUsers());
    }
}
