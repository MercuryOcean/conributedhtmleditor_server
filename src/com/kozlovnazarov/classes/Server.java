package com.kozlovnazarov.classes;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Server {

    private final static int PORT = 3124;
    private InetAddress ip = null;

    public Server() {
        ServerSocket serverSocket;
        Socket socket;

        try {
            ip = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }

        try {
            serverSocket = new ServerSocket(PORT, 0, ip );
            System.out.println("Server start");
            while(true) {
                socket = serverSocket.accept();
                System.out.println("Has connect");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
